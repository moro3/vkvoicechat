package com.moro.vkvoicechat.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.moro.vkvoicechat.R;
import com.moro.vkvoicechat.VkApiCallback;
import com.moro.vkvoicechat.VkVoiceChatActivity;
import com.moro.vkvoicechat.model.pojo.Message;
import com.moro.vkvoicechat.model.pojo.User;
import com.moro.vkvoicechat.model.pojo.container.GetDialogsAndProfilesResponseContainer;
import com.moro.vkvoicechat.model.pojo.container.ResponseEntity;
import com.moro.vkvoicechat.model.pojo.helpers.Dialog;
import com.moro.vkvoicechat.model.pojo.helpers.GroupDialog;
import com.moro.vkvoicechat.model.pojo.helpers.UserDialog;
import com.moro.vkvoicechat.model.util.ErrorCodes;
import com.moro.vkvoicechat.util.SharedPreferencesHelper;
import com.moro.vkvoicechat.util.logger.Logger;
import com.moro.vkvoicechat.vk.VkApi;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 06.01.14
 * Time: 16:36
 */
public class DialogListActivity extends VkVoiceChatActivity {

    private static final String TAG = DialogListActivity.class.getSimpleName();

    LayoutInflater inflater;
    private List<Dialog> dialogsList;
    private DialogListAdapter dialogListAdapter;
    private DialogListActivity.GetDialogsAndProfilesCallback callback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_list_activity);
        ListView dialogListView = (ListView) findViewById(R.id.dialogs_list_view);
        dialogsList = new ArrayList<Dialog>();
        dialogListAdapter = new DialogListAdapter(dialogsList);
        dialogListView.setAdapter(dialogListAdapter);
        dialogListView.setOnItemClickListener(dialogListAdapter);
        inflater = LayoutInflater.from(this);
        callback = new GetDialogsAndProfilesCallback();
    }

    @Override
    protected void onStart() {
        super.onStart();
        VkApi.messages.getDialogsAndProfiles(20, callback);
    }

    class DialogListAdapter extends BaseAdapter implements AdapterView.OnItemClickListener {

        List<Dialog> dialogsList;

        DialogListAdapter(List<Dialog> dialogsList) {
            this.dialogsList = dialogsList;
        }

        @Override
        public int getCount() {
            return dialogsList.size();

        }

        @Override
        public Object getItem(int position) {
            return dialogsList.get(position);

        }

        @Override
        public long getItemId(int position) {
            return position;

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            ViewHolder viewHolder;
            Dialog dialog = dialogsList.get(position);
            if (view == null) {
                view = inflater.inflate(R.layout.dialogs_list_item, null);
                viewHolder = new ViewHolder();
                viewHolder.dialogImageView = (ImageView) view.findViewById(R.id.dialog_image);
                viewHolder.userNameTextView = (TextView) view.findViewById(R.id.user_name);
                viewHolder.lastMessageTextView = (TextView) view.findViewById(R.id.last_message_text);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            ImageLoader.getInstance().displayImage(dialog.getDialogImageUrl(), viewHolder.dialogImageView);
            viewHolder.userNameTextView.setText(dialog.getDialogName());
            viewHolder.lastMessageTextView.setText(dialog.getMessage().getBody());

            return view;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent dialogIntent = new Intent(DialogListActivity.this, DialogActivity.class);
            Dialog dialog = dialogsList.get(position);
            dialogIntent.putExtra(DialogActivity.USER_NAME, dialog.getDialogName());
            if (dialog instanceof UserDialog) {
                dialogIntent.putExtra(DialogActivity.USER_ID, dialog.getMessage().getUserId());
                dialogIntent.putExtra(DialogActivity.USER_PIC, dialog.getDialogImageUrl());
            } else if (dialog instanceof GroupDialog) {
                dialogIntent.putExtra(DialogActivity.CHAT_ID, dialog.getMessage().getChatId());
                dialogIntent.putExtra(DialogActivity.CHAT_USER_PICS, ((GroupDialog) dialog).getChatUsers());
            }
            startActivity(dialogIntent);
        }
    }

    class ViewHolder {
        ImageView dialogImageView;
        TextView userNameTextView;
        TextView lastMessageTextView;
    }

//    class GetDialogsCallback extends VkApiCallback<GetDialogsResponseContainer> {
//
//        @Override
//        public void onSuccess(GetDialogsResponseContainer response) {
//            updateList(response);
//        }
//
//        @Override
//        public void onFailure(ResponseEntity.Error error) {
//            Logger.w(TAG, "getDialogs request returned with error " + error.getErrorMessage());
//            switch (error.getErrorCode()) {
//                case ErrorCodes.ACCESS_TOKEN_EXPIRED:
//                    Logger.w(TAG, "Need to acquire new access token");
//                    break;
//            }
//        }
//
//        @Override
//        public void onCacheReady(GetDialogsResponseContainer cacheResponse) {
//            updateList(cacheResponse);
//        }
//
//        private void updateList(GetDialogsResponseContainer response) {
//            List<Message> messagesList = response.getItems();
//            if (messagesList != null) {
//                dialogsList.clear();
//                dialogsList.addAll(messagesList);
//                dialogListAdapter.notifyDataSetChanged();
//            } else {
//                Toast.makeText(DialogListActivity.this, "No messages received, sorry", Toast.LENGTH_SHORT).show();
//            }
//        }
//    }

    class GetDialogsAndProfilesCallback extends VkApiCallback<GetDialogsAndProfilesResponseContainer> {

        @Override
        public void onSuccess(GetDialogsAndProfilesResponseContainer response) {
            User currentUser = response.getCurrentUser();
            SharedPreferencesHelper.saveUserPhoto50(currentUser.getPhoto50Url());
            SharedPreferencesHelper.saveUserPhoto100(currentUser.getPhoto100Url());
            SharedPreferencesHelper.saveUserPhoto200(currentUser.getPhoto200Url());
            SharedPreferencesHelper.saveUserName(currentUser.getFirstName(), currentUser.getLastName());
            updateList(response);
        }

        @Override
        public void onFailure(ResponseEntity.Error error) {
            Logger.w(TAG, "getDialogs request returned with error " + error.getErrorMessage());
            switch (error.getErrorCode()) {
                case ErrorCodes.ACCESS_TOKEN_EXPIRED:
                    Logger.w(TAG, "Need to acquire new access token");
                    break;
            }
        }

        @Override
        public void onCacheReady(GetDialogsAndProfilesResponseContainer cacheResponse) {
            updateList(cacheResponse);
        }

        private void updateList(GetDialogsAndProfilesResponseContainer response) {
            List<Message> messagesList = response.getMessages().getItems();
            List<User> usersList = response.getUsers();
            if (messagesList != null) {
                dialogsList.clear();
                for (int i = 0; i < messagesList.size(); i++) {
                    Message message = messagesList.get(i);
                    User user = usersList.get(i);
                    if (message.getChatActive() == null) {
                        dialogsList.add(new UserDialog(message, user));
                    } else {
                        dialogsList.add(new GroupDialog(message, getListOfChatUsersFromIds(message.getChatActive(), response.getChatUsers())));
                        usersList.add(i, new User()); // dummy user to keep correspondence between users list and dialogs list
                    }
                }
                dialogListAdapter.notifyDataSetChanged();
            } else {
                Toast.makeText(DialogListActivity.this, "No messages received, sorry", Toast.LENGTH_SHORT).show();
            }
        }

        private ArrayList<User> getListOfChatUsersFromIds(long[] ids, List<User> allChatUsers) {
            if (allChatUsers == null) return null;
            ArrayList<User> currentChatUsers = new ArrayList<User>(ids.length);
            List<User> allChatUsersCopy = new ArrayList<User>(allChatUsers);
            for (long userId : ids) {
                for (User chatUser : allChatUsersCopy) {
                    if (chatUser.getId() == userId) {
                        currentChatUsers.add(chatUser);
                        allChatUsersCopy.remove(chatUser);
                        break;
                    }
                }
            }
            return currentChatUsers;
        }
    }
}
