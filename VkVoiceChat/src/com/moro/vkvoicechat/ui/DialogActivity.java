package com.moro.vkvoicechat.ui;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.moro.vkvoicechat.R;
import com.moro.vkvoicechat.VkApiCallback;
import com.moro.vkvoicechat.VkVoiceChatActivity;
import com.moro.vkvoicechat.core.CacheManager;
import com.moro.vkvoicechat.core.VoiceRecorder;
import com.moro.vkvoicechat.model.pojo.Message;
import com.moro.vkvoicechat.model.pojo.User;
import com.moro.vkvoicechat.model.pojo.VoiceMessage;
import com.moro.vkvoicechat.model.pojo.container.GetHistoryResponseContainer;
import com.moro.vkvoicechat.model.pojo.container.PostMessageResponseContainer;
import com.moro.vkvoicechat.model.pojo.container.ResponseEntity;
import com.moro.vkvoicechat.net.Api;
import com.moro.vkvoicechat.net.FileDownloadCallback;
import com.moro.vkvoicechat.net.pojo.PostVoiceMessageResponseContainer;
import com.moro.vkvoicechat.util.MessageType;
import com.moro.vkvoicechat.util.SharedPreferencesHelper;
import com.moro.vkvoicechat.util.VkApiSettings;
import com.moro.vkvoicechat.util.logger.Logger;
import com.moro.vkvoicechat.vk.VkApi;
import com.nostra13.universalimageloader.core.ImageLoader;
import retrofit.RetrofitError;
import retrofit.mime.TypedFile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.view.MotionEvent.ACTION_DOWN;
import static android.view.MotionEvent.ACTION_UP;
import static com.moro.vkvoicechat.util.MessageType.*;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 08.01.14
 * Time: 13:40
 */
public class DialogActivity extends VkVoiceChatActivity implements View.OnTouchListener {

    private static final String TAG = DialogActivity.class.getSimpleName();

    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_PIC = "user_pic";
    public static final String CHAT_ID = "chat_id";
    public static final String CHAT_USER_PICS = "chat_user_pics";

    private VoiceRecorder voiceRecorder;
    private ArrayList messagesList;
    LayoutInflater inflater;
    private MessagesListAdapter messagesListAdapter;
    long recordTimestamp;
    private long userId;
    private String userName;
    private String userPicUrl;
    private long chatId;
    private List<User> chatUserPicsList;
    private SimpleDateFormat dateFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_activity);
        Button pushToTalk = (Button) findViewById(R.id.push_to_talk_button);
        pushToTalk.setOnTouchListener(this);
        voiceRecorder = new VoiceRecorder();
        voiceRecorder.onCreate(savedInstanceState);
        inflater = LayoutInflater.from(this);
        messagesList = new ArrayList();
        ListView messagesListView = (ListView) findViewById(R.id.message_list);
        messagesListView.setDivider(null);
        messagesListAdapter = new MessagesListAdapter(messagesList);
        messagesListView.setAdapter(messagesListAdapter);
        userId = getIntent().getLongExtra(USER_ID, -1);
        userName = getIntent().getStringExtra(USER_NAME);
        userPicUrl = getIntent().getStringExtra(USER_PIC);
        chatId = getIntent().getLongExtra(CHAT_ID, -1);
        chatUserPicsList = (List<User>) getIntent().getSerializableExtra(CHAT_USER_PICS);
        dateFormat = new SimpleDateFormat("H:mm");
        if (chatId == -1) {
            VkApi.messages.getHistory(20, userId, new GetHistoryCallback());
        } else {
            VkApi.messages.getChatHistory(20, chatId, new GetHistoryCallback());
        }

    }

    /**
     * Identifies voice messages by checking the message body and replaces Message object with VoiceMessage object
     *
     * @param messagesList
     * @return
     */
    private void processVoiceMessages(List messagesList) {
        String audioMessageTextPattern = "(\\(\\d+[,.]\\d+\\s.+\\) - " + VkApiSettings.VK_APP_URL_ESCAPED + "#)(\\d+)";
        Pattern p = Pattern.compile(audioMessageTextPattern);
        for (int i = 0; i < messagesList.size(); i++) {
            Message message = (Message) messagesList.get(i);
            String messageBody = message.getBody();
            Matcher m = p.matcher(messageBody);
            while (m.find()) {
                Logger.i(TAG, "Wow, it matches!");
                Logger.i(TAG, m.group());
                VoiceMessage voiceMessage = new VoiceMessage(message);
                voiceMessage.setVoiceMessageId(m.group(2));
                messagesList.remove(i);
                messagesList.add(i, voiceMessage);
            }
        }
    }


    /**
     * Describes Push-To-Talk button logic. Start recording on button press and stop it on button release.
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v.getId() == R.id.push_to_talk_button) {
            switch (event.getAction()) {
                case ACTION_DOWN:
                    recordTimestamp = System.currentTimeMillis();
                    try {
                        voiceRecorder.startRecording(CacheManager.getFilePathFromCache(String.valueOf(recordTimestamp)));
                    } catch (IOException e) {
                        Logger.e(TAG, e.getMessage(), e);
                    }
                    break;
                case ACTION_UP:
                    voiceRecorder.stopRecording();
                    final VoiceMessage voiceMessage = new VoiceMessage();
                    int duration = (int) (System.currentTimeMillis() - recordTimestamp);
                    voiceMessage.setMessageTimestamp(recordTimestamp);
                    voiceMessage.setDuration(duration);
                    messagesList.add(voiceMessage);
                    messagesListAdapter.notifyDataSetChanged();
                    Api.voiceMessage.save(SharedPreferencesHelper.loadUserId(), userId, duration,
                            new TypedFile("audio/ogg", new File(CacheManager.getFilePathFromCache(String.valueOf(recordTimestamp)))),
                            SharedPreferencesHelper.loadAccessToken(), new PostVoiceMessageCallback(voiceMessage));
                    break;
            }
        }
        return false;
    }

    class MessagesListAdapter extends BaseAdapter {

        ArrayList messagesList;

        MessagesListAdapter(ArrayList messageList) {
            this.messagesList = messageList;
        }

        @Override
        public int getCount() {
            return messagesList.size();

        }

        @Override
        public Object getItem(int position) {
            return messagesList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            Object message = messagesList.get(position);
            if (message instanceof VoiceMessage) {
                return MESSAGE_TYPE_VOICE.ordinal();
            } else if (message instanceof Message) {
                if (((Message) message).getFromId() != SharedPreferencesHelper.loadUserId()) {
                    return MESSAGE_TYPE_TEXT_TO_ME.ordinal();
                } else {
                    return MESSAGE_TYPE_TEXT_FROM_ME.ordinal();
                }
            }
            return MESSAGE_TYPE_TEXT_TO_ME.ordinal();
        }

        @Override
        public int getViewTypeCount() {
            return MessageType.values().length;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Object message = messagesList.get(position);
            Logger.d(TAG, "Position is " + position);
            if (convertView != null) {
                ViewHolder holder = (ViewHolder) convertView.getTag();
                Logger.d(TAG, "Holder text is " + (holder.messageView == null ? "null" : holder.messageView.getText()));
            }
            if (message instanceof VoiceMessage) {
                return getViewForVoiceMessage(convertView, (VoiceMessage) message);
            }
            if (message instanceof Message) {
                Logger.d(TAG, "Message text is  " + ((Message) message).getBody());
                boolean isMessageFromMe = getItemViewType(position) == MESSAGE_TYPE_TEXT_FROM_ME.ordinal();
                boolean isPreviousFromSameUser = position > 0 &&
                        ((Message) message).getFromId().equals(((Message) messagesList.get(position - 1)).getFromId());
                return getViewForMessage(convertView, (Message) message, isMessageFromMe, isPreviousFromSameUser);
            }
            Logger.e(TAG, "Couldn't find Message in list, error!");
            return convertView;
        }

        private View getViewForVoiceMessage(View view, VoiceMessage voiceMessage) {
            ViewHolder viewHolder;
            if (view == null) {
                view = inflater.inflate(R.layout.voice_message_item, null);
                viewHolder = new ViewHolder();
                viewHolder.voiceMessagePlayButton = (Button) view.findViewById(R.id.play_voice_message_button);
                viewHolder.postVoiceMessageToVkButton = (Button) view.findViewById(R.id.post_voice_message_to_vk);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            viewHolder.voiceMessagePlayButton.setOnClickListener(new PlayMessageButtonOnClickListener(voiceMessage));
            if (voiceMessage.getVoiceMessageId() != null) {
                viewHolder.voiceMessagePlayButton.setEnabled(true);
            } else {
                viewHolder.voiceMessagePlayButton.setEnabled(false);
            }
            viewHolder.postVoiceMessageToVkButton.setOnClickListener(new PostMessageToVkButtonOnClickListener(voiceMessage));
            if (voiceMessage.getVoiceMessageId() != null && voiceMessage.getMessageId() == null) {
                viewHolder.postVoiceMessageToVkButton.setVisibility(View.VISIBLE);
            } else {
                viewHolder.postVoiceMessageToVkButton.setVisibility(View.GONE);
            }
            return view;

        }

        private View getViewForMessage(View view, Message message, boolean isFromMe, boolean isPreviousFromSameUser) {
            ViewHolder viewHolder;
            if (view == null) {
                view = isFromMe ? inflater.inflate(R.layout.message_item_from_me, null) : inflater.inflate(R.layout.message_item_to_me, null);
                viewHolder = new ViewHolder();
                viewHolder.userPicView = (ImageView) view.findViewById(R.id.user_pic);
                viewHolder.messageTimeView = (TextView) view.findViewById(R.id.message_time);
                viewHolder.messageView = (TextView) view.findViewById(R.id.message_text);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            viewHolder.messageTimeView.setText(dateFormat.format(message.getDate() * 1000));
            if (!isFromMe) {
                if (chatUserPicsList == null) {
                    ImageLoader.getInstance().displayImage(userPicUrl, viewHolder.userPicView);
                } else {
                    ImageLoader.getInstance().displayImage(getChatUserPicUrl(message.getUserId(), chatUserPicsList), viewHolder.userPicView);
                }
            } else {
                ImageLoader.getInstance().displayImage(SharedPreferencesHelper.loadUserPhoto100(), viewHolder.userPicView);
            }
            viewHolder.userPicView.setVisibility(isPreviousFromSameUser ? View.INVISIBLE : View.VISIBLE);
            viewHolder.messageView.setText(message.getBody());
            return view;
        }

        class ViewHolder {
            TextView messageView;
            Button voiceMessagePlayButton;
            Button postVoiceMessageToVkButton;
            ImageView userPicView;
            TextView messageTimeView;
        }

        private String getChatUserPicUrl(long userId, List<User> chatUsersList) {
            for (User chatUser : chatUsersList) {
                if (chatUser.getId() == userId) {
                    return chatUser.getPhoto100Url();
                }
            }
            return null;
        }

        class PlayMessageButtonOnClickListener implements View.OnClickListener {

            private final MediaPlayer mediaPlayer;
            private VoiceMessage voiceMessage;

            PlayMessageButtonOnClickListener(VoiceMessage voiceMessage) {
                mediaPlayer = new MediaPlayer();
                this.voiceMessage = voiceMessage;
            }

            @Override
            public void onClick(View v) {
                downloadVoiceMessage(voiceMessage);
            }

            private void downloadVoiceMessage(final VoiceMessage voiceMessage) {
                Api.voiceMessage.download(String.valueOf(voiceMessage.getFromId()), voiceMessage.getVoiceMessageId(), SharedPreferencesHelper.loadAccessToken(),
                        new FileDownloadCallback() {
                            @Override
                            public void onSuccess() {
                                Logger.d(TAG, "Successfully downloaded voice message");
                                String voiceMessageFilePath = CacheManager.getFilePathFromCache(voiceMessage.getVoiceMessageId());
                                playVoiceMessage(voiceMessageFilePath);
                            }

                            @Override
                            public void onFailure(RetrofitError error) {
                                Logger.d(TAG, "Error while downloading voice message " + error.getMessage());
                            }

                            @Override
                            public void onCacheReady(String filePath) {
                                Logger.d(TAG, "Got voice message from cache");
                                playVoiceMessage(filePath);
                            }

                            private void playVoiceMessage(String voiceMessageFilePath) {
                                try {
                                    mediaPlayer.reset();
                                    mediaPlayer.setDataSource(voiceMessageFilePath);
                                    mediaPlayer.prepare();
                                    mediaPlayer.start();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
            }
        }

        class PostMessageToVkButtonOnClickListener implements View.OnClickListener {

            private VoiceMessage voiceMessage;

            PostMessageToVkButtonOnClickListener(VoiceMessage voiceMessage) {
                this.voiceMessage = voiceMessage;
            }

            @Override
            public void onClick(View v) {
                String messageText = getResources().getString(R.string.audio_message_text, voiceMessage.getDuration() / 1000,
                        VkApiSettings.VK_APP_URL, voiceMessage.getVoiceMessageId());
                Logger.d(TAG, "Compiled message text is " + messageText);
                VkApi.messages.send(userId, messageText, new VkApiCallback<PostMessageResponseContainer>() {
                    @Override
                    public void onSuccess(PostMessageResponseContainer response) {
                        Logger.d(TAG, "Successfully posted a message with id " + response.getMessageId());
                    }

                    @Override
                    public void onFailure(ResponseEntity.Error error) {
                        Logger.d(TAG, "Error posting message: " + error.getErrorMessage());
                    }
                });
            }
        }
    }

    class GetHistoryCallback extends VkApiCallback<GetHistoryResponseContainer> {

        @Override
        public void onSuccess(GetHistoryResponseContainer response) {
            updateList(response, false);
        }

        @Override
        public void onFailure(ResponseEntity.Error error) {
            Toast.makeText(DialogActivity.this, "Error: " + error.getErrorMessage(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCacheReady(GetHistoryResponseContainer cacheResponse) {
            updateList(cacheResponse, true);
        }


        private void updateList(GetHistoryResponseContainer response, boolean isFromCache) {
            List messageHistoryList = response.getItems();
            processVoiceMessages(messageHistoryList);
            if (messageHistoryList != null) {
                messagesList.clear();
                if (!isFromCache) {
                    Collections.reverse(messageHistoryList);
                }
                messagesList.addAll(messageHistoryList);
                messagesListAdapter.notifyDataSetChanged();
            } else {
                Toast.makeText(DialogActivity.this, "No messages received, sorry", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class PostVoiceMessageCallback extends VkApiCallback<PostVoiceMessageResponseContainer> {

        VoiceMessage voiceMessage;

        PostVoiceMessageCallback(VoiceMessage voiceMessage) {
            this.voiceMessage = voiceMessage;
        }

        @Override
        public void onSuccess(PostVoiceMessageResponseContainer response) {
            Logger.d(TAG, "Got successful post voice message callback");
            Logger.i(TAG, "Received response:");
            Logger.i(TAG, "id:" + response.getId());
            Logger.i(TAG, "name:" + response.getName());
            Logger.i(TAG, "url:" + response.getUrl());
            voiceMessage.setVoiceMessageId(response.getId());
            messagesListAdapter.notifyDataSetChanged();

        }

        @Override
        public void onFailure(ResponseEntity.Error error) {
            Logger.i(TAG, "Received error:");
            Logger.i(TAG, "error_code:" + error.getErrorCode());
            Logger.i(TAG, "error_msg:" + error.getErrorMessage());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        voiceRecorder.onDestroy();
    }
}
