package com.moro.vkvoicechat.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.moro.vkvoicechat.R;
import com.moro.vkvoicechat.VkVoiceChatActivity;
import com.moro.vkvoicechat.util.VkApiException;
import com.moro.vkvoicechat.util.logger.Logger;
import com.moro.vkvoicechat.vk.VkApi;
import com.moro.vkvoicechat.vk.VkAuth;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 09.01.14
 * Time: 14:38
 */
public class VkAuthActivity extends VkVoiceChatActivity {
    private static final String TAG = VkAuthActivity.class.getSimpleName();

    public static final String ACCESS_TOKEN = "access_token";
    public static final String USER_ID = "user_id";

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        WebView loginWebView = (WebView) findViewById(R.id.login_web_view);
        loginWebView.getSettings().setJavaScriptEnabled(true);
        loginWebView.clearCache(true);

//        CookieSyncManager.createInstance(this);
//        CookieManager cookieManager = CookieManager.getInstance();
//        cookieManager.removeAllCookie();

        loginWebView.setWebViewClient(new LoginWebViewClient());
        loginWebView.loadUrl(VkAuth.getAuthRequestUrl());
    }

    class LoginWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            try {
                parseUrl(url);
            } catch (VkApiException e) {
                Logger.e(TAG, e.getMessage(), e);
            }
        }
    }

    private void parseUrl(String url) throws VkApiException {
        if (url == null) return;
        Logger.i(TAG, "url=" + url);
        if (url.startsWith(VkAuth.REDIRECT_URI)) {
            Intent authCompletedIntent = new Intent();
            if (!url.contains("error=")) {
                String[] auth = VkAuth.parseRedirectUri(url);
                VkApi.setAccessToken(auth[0]);
                authCompletedIntent.putExtra(ACCESS_TOKEN, auth[0]);
                authCompletedIntent.putExtra(USER_ID, auth[1]);
                setResult(RESULT_OK, authCompletedIntent);
            } else {
                setResult(RESULT_CANCELED);
            }
            finish();
        }

    }
}
