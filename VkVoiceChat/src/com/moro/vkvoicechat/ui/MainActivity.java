package com.moro.vkvoicechat.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import com.moro.vkvoicechat.R;
import com.moro.vkvoicechat.VkVoiceChatActivity;
import com.moro.vkvoicechat.net.Api;
import com.moro.vkvoicechat.util.SharedPreferencesHelper;
import com.moro.vkvoicechat.util.logger.Logger;
import com.moro.vkvoicechat.vk.VkApi;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends VkVoiceChatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    public static final int VK_AUTH_REQUEST = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (!VkApi.isInited()) {
            requestAuthentication(this);
        }
    }

    public void requestAuthentication(Context context) {
        Intent authIntent = new Intent(context, VkAuthActivity.class);
        startActivityForResult(authIntent, VK_AUTH_REQUEST);
    }

    public void onClick(View view) {
        startActivity(new Intent(this, DialogListActivity.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VK_AUTH_REQUEST) {
            if (resultCode == RESULT_OK) {
                Logger.i(TAG, "Vk Api is successfully inited, access token: " + VkApi.getAccessToken());
                SharedPreferencesHelper.saveAccessToken(data.getStringExtra(VkAuthActivity.ACCESS_TOKEN));
                SharedPreferencesHelper.saveUserId(Long.parseLong(data.getStringExtra(VkAuthActivity.USER_ID)));
                Api.token.register(data.getStringExtra(VkAuthActivity.USER_ID), data.getStringExtra(VkAuthActivity.ACCESS_TOKEN), Build.VERSION.SDK_INT, new Callback<Response>() {
                    @Override
                    public void success(Response response, Response response2) {

                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
            } else {
                Logger.w(TAG, "Vk Auth was cancelled");
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
