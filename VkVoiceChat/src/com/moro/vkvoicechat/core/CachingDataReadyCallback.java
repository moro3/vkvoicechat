package com.moro.vkvoicechat.core;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 23.01.14
 * Time: 19:39
 */
public interface CachingDataReadyCallback<T> {

    public void cacheData(T dataContainer);

}
