package com.moro.vkvoicechat.core;

import android.media.MediaRecorder;
import android.os.Bundle;

import java.io.IOException;

import static android.media.MediaRecorder.OnErrorListener;
import static android.media.MediaRecorder.OnInfoListener;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 08.01.14
 * Time: 14:29
 */
public class VoiceRecorder {

    MediaRecorder mediaRecorder;
    OnInfoListener onInfoListener;
    OnErrorListener onErrorListener;

    public VoiceRecorder(OnInfoListener onInfoListener, OnErrorListener onErrorListener) {
        this.onInfoListener = onInfoListener;
        this.onErrorListener = onErrorListener;
    }

    public VoiceRecorder() { }

    public void onCreate(Bundle savedInstanceState) {
        mediaRecorder = new MediaRecorder();
    }

    public void startRecording(String toFilePath) throws IOException {
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        mediaRecorder.setOutputFile(toFilePath);
        mediaRecorder.setOnInfoListener(onInfoListener);
        mediaRecorder.setOnErrorListener(onErrorListener);
        mediaRecorder.prepare();
        mediaRecorder.start();
    }

    public void stopRecording() {
        mediaRecorder.stop();
        mediaRecorder.reset();
    }

    public void onDestroy() {
        mediaRecorder.release();
    }

}
