package com.moro.vkvoicechat.core;

import android.content.Context;
import android.os.Environment;
import com.google.common.io.ByteStreams;
import com.moro.vkvoicechat.VkVoiceChatApplication;
import com.moro.vkvoicechat.model.dao.*;
import com.moro.vkvoicechat.model.pojo.container.GetDialogsAndProfilesResponseContainer;
import com.moro.vkvoicechat.model.pojo.container.GetDialogsResponseContainer;
import com.moro.vkvoicechat.model.pojo.container.GetHistoryResponseContainer;
import com.moro.vkvoicechat.net.pojo.PostVoiceMessageResponseContainer;
import com.moro.vkvoicechat.util.logger.Logger;
import org.apache.commons.lang3.SerializationException;
import org.apache.commons.lang3.SerializationUtils;
import retrofit.client.Response;

import java.io.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 08.01.14
 * Time: 14:52
 */
public class CacheManager {

    private static final String TAG = CacheManager.class.getSimpleName();

    static Context context;
    static DaoSession daoSession;

    public static void init() {
        context = VkVoiceChatApplication.getAppContext();
        daoSession = new DaoMaster(new DaoMaster.DevOpenHelper(context, "cache-db", null).getWritableDatabase()).newSession();
    }

    public static String getFilePathFromCache(String fileName) {
        File storageDir = context.getExternalCacheDir();
        if (storageDir == null) {
            storageDir = context.getExternalFilesDir(Environment.DIRECTORY_MUSIC);
        }
        if (storageDir.mkdirs()) {
            Logger.d(TAG, "Cache dir created");
        } else {
            Logger.w(TAG, "File path is not created or already exists");
        }
        return storageDir.getPath() + File.separatorChar + fileName;
    }

    private static DaoSession getDaoSession() {
        return daoSession;
    }

    public static void addVoiceMessageToCache(PostVoiceMessageResponseContainer container, String tempVoiceMessageFileName) {
        VoiceMessage voiceMessage = new VoiceMessage();
        voiceMessage.setVoiceMessageId(container.getId());
        voiceMessage.setPathToFile(getFilePathFromCache(container.getId()));
        File voiceMessageFile = new File(getFilePathFromCache(tempVoiceMessageFileName));
        if (voiceMessageFile.renameTo(new File(voiceMessage.getPathToFile()))) {
            Logger.i(TAG, "Successfully renamed voice message file");
        } else {
            Logger.i(TAG, "Error renaming voice message file");
        }
        getDaoSession().getVoiceMessageDao().insertOrReplace(voiceMessage);
    }

    public static void addVoiceMessageFileToCache(Response voiceMessageFileResponse, String voiceMessageFileName) {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = voiceMessageFileResponse.getBody().in();
            String voiceMessageFilePath = getFilePathFromCache(voiceMessageFileName);
            out = new FileOutputStream(voiceMessageFilePath);
            ByteStreams.copy(in, out);
            VoiceMessage voiceMessage = new VoiceMessage();
            voiceMessage.setVoiceMessageId(voiceMessageFileName);
            voiceMessage.setPathToFile(voiceMessageFilePath);
            getDaoSession().getVoiceMessageDao().insertOrReplace(voiceMessage);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getVoiceMessageFileFromCache(String voiceMessageId) {
        List<VoiceMessage> voiceMessageList = getDaoSession().getVoiceMessageDao().queryBuilder().where(VoiceMessageDao.Properties.VoiceMessageId.eq(voiceMessageId)).build().list();
        if (!voiceMessageList.isEmpty()) {
            VoiceMessage voiceMessage = voiceMessageList.get(0);
            if (voiceMessage != null) {
                return voiceMessage.getPathToFile();
            } else {
                return null;

            }
        }
        return null;
    }

    public static void saveGetDialogsToDb(GetDialogsResponseContainer container, long offset, int count, int previewLength, long userId) {
        // Need to delete the old data because response is always a different object and insertOrReplace always inserts new row
        getDaoSession().getMessagesGetDialogsDao().deleteInTx(queryForMessagesGetDialogs(offset, count, previewLength, userId));
        getDaoSession().getMessagesGetDialogsDao().insertOrReplace(new MessagesGetDialogs(null, offset, count, previewLength, userId,
                SerializationUtils.serialize(container)));
    }

    public static GetDialogsResponseContainer loadGetDialogsFromDb(long offset, int count, int previewLength, long userId) {
        GetDialogsResponseContainer container = null;
        List<MessagesGetDialogs> responsesList = queryForMessagesGetDialogs(offset, count, previewLength, userId);
        if (!responsesList.isEmpty()) {
            container = SerializationUtils.deserialize(responsesList.get(0).getResponse());
        }
        return container;
    }

    private static List<MessagesGetDialogs> queryForMessagesGetDialogs(long offset, int count, int previewLength, long userId) {
        return getDaoSession().getMessagesGetDialogsDao().queryBuilder().where(
                    MessagesGetDialogsDao.Properties.Offset.eq(offset),
                    MessagesGetDialogsDao.Properties.Count.eq(count),
                    MessagesGetDialogsDao.Properties.PreviewLength.eq(previewLength),
                    MessagesGetDialogsDao.Properties.UserId.eq(userId))
                    .build().list();
    }

    public static void saveGetDialogsAndProfilesToDb(GetDialogsAndProfilesResponseContainer container, long offset, int count, int previewLength, long userId) {
        // Need to delete the old data because response is always a different object and insertOrReplace always inserts new row
        getDaoSession().getExecuteGetDialogsAndProfilesDao().deleteInTx(queryForExecuteGetDialogsAndProfiles(offset, count, previewLength, userId));
        getDaoSession().getExecuteGetDialogsAndProfilesDao().insertOrReplace(new ExecuteGetDialogsAndProfiles(null, offset, count, previewLength, userId,
                SerializationUtils.serialize(container)));
    }

    public static GetDialogsAndProfilesResponseContainer loadGetDialogsAndProfilesFromDb(long offset, int count, int previewLength, long userId) {
        GetDialogsAndProfilesResponseContainer container = null;
        List<ExecuteGetDialogsAndProfiles> responsesList = queryForExecuteGetDialogsAndProfiles(offset, count, previewLength, userId);
        if (!responsesList.isEmpty()) {
            try {
                container = SerializationUtils.deserialize(responsesList.get(0).getResponse());
            } catch (SerializationException e) {
                Logger.e(TAG, e.getMessage(), e);
            }
        }
        return container;
    }

    private static List<ExecuteGetDialogsAndProfiles> queryForExecuteGetDialogsAndProfiles(long offset, int count, int previewLength, long userId) {
        return getDaoSession().getExecuteGetDialogsAndProfilesDao().queryBuilder().where(
                ExecuteGetDialogsAndProfilesDao.Properties.Offset.eq(offset),
                ExecuteGetDialogsAndProfilesDao.Properties.Count.eq(count),
                ExecuteGetDialogsAndProfilesDao.Properties.PreviewLength.eq(previewLength),
                ExecuteGetDialogsAndProfilesDao.Properties.UserId.eq(userId))
                .build().list();
    }

    public static void saveGetHistoryToDb(GetHistoryResponseContainer container, long offset, int count, long userId, long startMessageId, int rev) {
        // Need to delete the old data because response is always a different object and insertOrReplace always inserts new row
        getDaoSession().getMessagesGetHistoryDao().deleteInTx(queryForMessagesGetHistory(offset, count, userId, startMessageId, rev));
        getDaoSession().getMessagesGetHistoryDao().insert(new MessagesGetHistory(null, offset, count, userId,
                startMessageId, rev, SerializationUtils.serialize(container)));
    }

    public static GetHistoryResponseContainer loadGetHistoryFromDb(long offset, int count, long userId,
                                                                   long startMessageId, int rev) {
        GetHistoryResponseContainer container = null;
        List<MessagesGetHistory> responsesList = queryForMessagesGetHistory(offset, count, userId, startMessageId, rev);
        if (!responsesList.isEmpty()) {
            try {
                container = SerializationUtils.deserialize(responsesList.get(0).getResponse());
            } catch (SerializationException e) {
                Logger.e(TAG, e.getMessage(), e);
            }
        }
        return container;
    }

    private static List<MessagesGetHistory> queryForMessagesGetHistory(long offset, int count, long userId, long startMessageId, int rev) {
        return getDaoSession().getMessagesGetHistoryDao().queryBuilder().where(
                    MessagesGetHistoryDao.Properties.Offset.eq(offset),
                    MessagesGetHistoryDao.Properties.Count.eq(count),
                    MessagesGetHistoryDao.Properties.UserId.eq(userId),
                    MessagesGetHistoryDao.Properties.StartMessageId.eq(startMessageId),
                    MessagesGetHistoryDao.Properties.Rev.eq(rev))
                    .build().list();
    }
}
