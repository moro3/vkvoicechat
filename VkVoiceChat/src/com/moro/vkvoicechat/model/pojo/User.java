package com.moro.vkvoicechat.model.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 28.01.14
 * Time: 17:23
 */
public class User implements Serializable {

    private long id;

    @JsonProperty(value = "first_name")
    private String firstName;

    @JsonProperty(value = "last_name")
    private String lastName;

    private int sex;

    @JsonProperty(value = "photo_50")
    private String photo50Url;

    @JsonProperty(value = "photo_100")
    private String photo100Url;

    @JsonProperty(value = "photo_200")
    private String photo200Url;

    @JsonProperty(value = "photo_max_orig")
    private String photoMaxOrigUrl;

    private int online;

    @JsonProperty(value = "online_app")
    private long onlineApp;

    @JsonProperty(value = "online_mobile")
    private long onlineMobile;

    @JsonProperty(value = "can_post")
    private int canPost;

    @JsonProperty(value = "can_write_private_message")
    private String canWritePrivateMessage;

    @JsonProperty(value = "last_seen")
    private LastSeenProperty lastSeen;

    private String deactivated;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getPhoto50Url() {
        return photo50Url;
    }

    public void setPhoto50Url(String photo50Url) {
        this.photo50Url = photo50Url;
    }

    public String getPhoto100Url() {
        return photo100Url;
    }

    public void setPhoto100Url(String photo100Url) {
        this.photo100Url = photo100Url;
    }

    public String getPhoto200Url() {
        return photo200Url;
    }

    public void setPhoto200Url(String photo200Url) {
        this.photo200Url = photo200Url;
    }

    public String getPhotoMaxOrigUrl() {
        return photoMaxOrigUrl;
    }

    public void setPhotoMaxOrigUrl(String photoMaxOrigUrl) {
        this.photoMaxOrigUrl = photoMaxOrigUrl;
    }

    public int getOnline() {
        return online;
    }

    public void setOnline(int online) {
        this.online = online;
    }

    public long getOnlineApp() {
        return onlineApp;
    }

    public void setOnlineApp(long onlineApp) {
        this.onlineApp = onlineApp;
    }

    public long getOnlineMobile() {
        return onlineMobile;
    }

    public void setOnlineMobile(long onlineMobile) {
        this.onlineMobile = onlineMobile;
    }

    public int getCanPost() {
        return canPost;
    }

    public void setCanPost(int canPost) {
        this.canPost = canPost;
    }

    public String getCanWritePrivateMessage() {
        return canWritePrivateMessage;
    }

    public void setCanWritePrivateMessage(String canWritePrivateMessage) {
        this.canWritePrivateMessage = canWritePrivateMessage;
    }

    public LastSeenProperty getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(LastSeenProperty lastSeen) {
        this.lastSeen = lastSeen;
    }

    public String getDeactivated() {
        return deactivated;
    }

    public void setDeactivated(String deactivated) {
        this.deactivated = deactivated;
    }

    static class LastSeenProperty implements Serializable {

        private long time;
        private int platform;

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }

        public int getPlatform() {
            return platform;
        }

        public void setPlatform(int platform) {
            this.platform = platform;
        }
    }

}
