package com.moro.vkvoicechat.model.pojo.helpers;

import com.moro.vkvoicechat.model.pojo.Message;
import com.moro.vkvoicechat.model.pojo.User;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 02.03.14
 * Time: 17:00
 */
public class UserDialog extends Dialog {

    private Message message;
    private User user;

    public UserDialog(Message message, User user) {
        this.message = message;
        this.user = user;
    }

    @Override
    public Message getMessage() {
        return message;

    }

    @Override
    public String getDialogImageUrl() {
        return user.getPhoto200Url();

    }

    @Override
    public String getDialogName() {
        return user.getFirstName() + " " + user.getLastName();
    }
}
