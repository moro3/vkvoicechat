package com.moro.vkvoicechat.model.pojo.helpers;

import com.moro.vkvoicechat.model.pojo.Message;
import com.moro.vkvoicechat.model.pojo.User;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 02.03.14
 * Time: 17:06
 */
public class GroupDialog extends Dialog {

    private Message message;
    private ArrayList<User> chatUsers;

    public GroupDialog(Message message, ArrayList<User> chatUsers) {
        this.message = message;
        this.chatUsers = chatUsers;
    }

    @Override
    public Message getMessage() {
        return message;

    }

    @Override
    public String getDialogImageUrl() {
        return message.getPhoto200();

    }

    @Override
    public String getDialogName() {
        return message.getTitle();
    }

    public ArrayList<User> getChatUsers() {
        return chatUsers;
    }
}
