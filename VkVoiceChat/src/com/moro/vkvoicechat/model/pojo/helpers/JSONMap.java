package com.moro.vkvoicechat.model.pojo.helpers;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 16.01.14
 * Time: 13:09
 */
public class JSONMap {
    String key;
    String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
