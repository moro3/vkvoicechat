package com.moro.vkvoicechat.model.pojo.helpers;

import com.moro.vkvoicechat.model.pojo.Message;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 28.01.14
 * Time: 19:21
 */
public abstract class Dialog {

    public abstract Message getMessage();

    public abstract String getDialogImageUrl();

    public abstract String getDialogName();
}
