package com.moro.vkvoicechat.model.pojo;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 08.01.14
 * Time: 16:27
 */
public class VoiceMessage extends Message {

    private long messageTimestamp;
    private String voiceMessageId;
    private double duration;

    public VoiceMessage(Message message) {
        super(message.getId(), message.getMessageId(), message.getUserId(), message.getFromId(), message.getDate(), message.getReadState(), message.getOut(), message.getTitle(), message.getBody(), message.getAttachments(), message.getFwdMessages(), message.getEmoji(), message.getImportant(), message.getDeleted(), message.getChatId(), message.getChatActive(), message.getUsersCount(), message.getAdminId(), message.getPhoto50(), message.getPhoto100(), message.getPhoto200());
    }

    public VoiceMessage() {}

    public void setMessageTimestamp(long messageTimestamp) {
        this.messageTimestamp = messageTimestamp;
    }

    public long getMessageTimestamp() {
        return messageTimestamp;
    }

    public String getMessageTimestampAsString() {
        return String.valueOf(messageTimestamp);
    }

    public String getVoiceMessageId() {
        return voiceMessageId;
    }

    public void setVoiceMessageId(String voiceMessageId) {
        this.voiceMessageId = voiceMessageId;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }
}
