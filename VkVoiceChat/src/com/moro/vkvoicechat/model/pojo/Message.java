package com.moro.vkvoicechat.model.pojo;


public class Message implements java.io.Serializable {

    private Long id;
    private Long messageId;
    private Long userId;
    private Long fromId;
    private Long date;
    private Integer readState;
    private Integer out;
    private String title;
    private String body;
    private byte[] attachments;
    private byte[] fwdMessages;
    private Integer emoji;
    private Integer important;
    private Integer deleted;
    private Long chatId;
    private long[] chatActive;
    private Long usersCount;
    private Long adminId;
    private String photo50;
    private String photo100;
    private String photo200;

    public Message() {
    }

    public Message(Long id) {
        this.id = id;
    }

    public Message(Long id, Long messageId, Long userId, Long fromId, Long date, Integer readState, Integer out, String title, String body, byte[] attachments, byte[] fwdMessages, Integer emoji, Integer important, Integer deleted, Long chatId, long[] chatActive, Long usersCount, Long adminId, String photo50, String photo100, String photo200) {
        this.id = id;
        this.messageId = messageId;
        this.userId = userId;
        this.fromId = fromId;
        this.date = date;
        this.readState = readState;
        this.out = out;
        this.title = title;
        this.body = body;
        this.attachments = attachments;
        this.fwdMessages = fwdMessages;
        this.emoji = emoji;
        this.important = important;
        this.deleted = deleted;
        this.chatId = chatId;
        this.chatActive = chatActive;
        this.usersCount = usersCount;
        this.adminId = adminId;
        this.photo50 = photo50;
        this.photo100 = photo100;
        this.photo200 = photo200;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getFromId() {
        return fromId;
    }

    public void setFromId(Long fromId) {
        this.fromId = fromId;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Integer getReadState() {
        return readState;
    }

    public void setReadState(Integer readState) {
        this.readState = readState;
    }

    public Integer getOut() {
        return out;
    }

    public void setOut(Integer out) {
        this.out = out;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public byte[] getAttachments() {
        return attachments;
    }

    public void setAttachments(byte[] attachments) {
        this.attachments = attachments;
    }

    public byte[] getFwdMessages() {
        return fwdMessages;
    }

    public void setFwdMessages(byte[] fwdMessages) {
        this.fwdMessages = fwdMessages;
    }

    public Integer getEmoji() {
        return emoji;
    }

    public void setEmoji(Integer emoji) {
        this.emoji = emoji;
    }

    public Integer getImportant() {
        return important;
    }

    public void setImportant(Integer important) {
        this.important = important;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public long[] getChatActive() {
        return chatActive;
    }

    public void setChatActive(long[] chatActive) {
        this.chatActive = chatActive;
    }

    public Long getUsersCount() {
        return usersCount;
    }

    public void setUsersCount(Long usersCount) {
        this.usersCount = usersCount;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public String getPhoto50() {
        return photo50;
    }

    public void setPhoto50(String photo50) {
        this.photo50 = photo50;
    }

    public String getPhoto100() {
        return photo100;
    }

    public void setPhoto100(String photo100) {
        this.photo100 = photo100;
    }

    public String getPhoto200() {
        return photo200;
    }

    public void setPhoto200(String photo200) {
        this.photo200 = photo200;
    }

}
