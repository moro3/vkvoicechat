package com.moro.vkvoicechat.model.pojo;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 15.01.14
 * Time: 15:52
 */
@Deprecated
public class ErrorEntity {

    Error error;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    class Error {

        int errorCode;
        String errorMessage;
        Map<String, String> requestParams;

        public int getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(int errorCode) {
            this.errorCode = errorCode;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public Map<String, String> getRequestParams() {
            return requestParams;
        }

        public void setRequestParams(Map<String, String> requestParams) {
            this.requestParams = requestParams;
        }
    }

}
