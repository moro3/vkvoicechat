package com.moro.vkvoicechat.model.pojo.container;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 20.01.14
 * Time: 15:32
 *
 * This container is a hack, because VK sends response in format {"response":39843} instead of {"response":{"message_id":39843}}
 */
public class PostMessageResponseContainer extends Number implements ResponseContainer {

    public long response;

    public long getMessageId() {
        return response;
    }

    public void setMessageId(long messageId) {
        this.response = messageId;
    }

    public PostMessageResponseContainer(long response) {
        this.response = response;
    }

    @Override
    public double doubleValue() {
        return response;

    }

    @Override
    public float floatValue() {
        return response;

    }

    @Override
    public int intValue() {
        return (int) response;

    }

    @Override
    public long longValue() {
        return response;

    }
}
