package com.moro.vkvoicechat.model.pojo.container;

import com.moro.vkvoicechat.model.pojo.Message;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 14.01.14
 * Time: 16:54
 */
public class GetHistoryResponseContainer implements ResponseContainer {

    int count;
    List<Message> items;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Message> getItems() {
        return items;
    }

    public void setItems(List<Message> items) {
        this.items = items;
    }

}
