package com.moro.vkvoicechat.model.pojo.container;

import com.moro.vkvoicechat.model.pojo.helpers.JSONMap;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 15.01.14
 * Time: 15:52
 */
public class ResponseEntity<E> {

    E response;

    public E getResponse() {
        return response;
    }

    public void setResponse(E response) {
        this.response = response;
    }

    Error error;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public static class Error {

        int errorCode;
        String errorMessage;
        List<JSONMap> requestParams;

        public int getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(int errorCode) {
            this.errorCode = errorCode;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public List<JSONMap> getRequestParams() {
            return requestParams;
        }

        public void setRequestParams(List<JSONMap> requestParams) {
            this.requestParams = requestParams;
        }
    }
}
