package com.moro.vkvoicechat.model.pojo.container;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.moro.vkvoicechat.model.pojo.User;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 28.01.14
 * Time: 17:15
 */
public class GetDialogsAndProfilesResponseContainer implements ResponseContainer {

    @JsonProperty(value = "current_user")
    ArrayList<User> currentUserList;

    GetDialogsResponseContainer messages;

    ArrayList<User> users;

    @JsonProperty(value = "cusers")
    ArrayList<User> chatUsers;

    long time;

    public ArrayList<User> getCurrentUserList() {
        return currentUserList;
    }

    public void setCurrentUserList(ArrayList<User> currentUserList) {
        this.currentUserList = currentUserList;
    }

    public User getCurrentUser() {
        return currentUserList.get(0);
    }

    public GetDialogsResponseContainer getMessages() {
        return messages;
    }

    public void setMessages(GetDialogsResponseContainer messages) {
        this.messages = messages;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    public ArrayList<User> getChatUsers() {
        return chatUsers;
    }

    public void setChatUsers(ArrayList<User> chatUsers) {
        this.chatUsers = chatUsers;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
