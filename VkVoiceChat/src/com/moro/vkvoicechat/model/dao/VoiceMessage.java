package com.moro.vkvoicechat.model.dao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table VOICE_MESSAGE.
 */
public class VoiceMessage implements java.io.Serializable {

    private Long id;
    private String voiceMessageId;
    private String pathToFile;

    public VoiceMessage() {
    }

    public VoiceMessage(Long id) {
        this.id = id;
    }

    public VoiceMessage(Long id, String voiceMessageId, String pathToFile) {
        this.id = id;
        this.voiceMessageId = voiceMessageId;
        this.pathToFile = pathToFile;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVoiceMessageId() {
        return voiceMessageId;
    }

    public void setVoiceMessageId(String voiceMessageId) {
        this.voiceMessageId = voiceMessageId;
    }

    public String getPathToFile() {
        return pathToFile;
    }

    public void setPathToFile(String pathToFile) {
        this.pathToFile = pathToFile;
    }

}
