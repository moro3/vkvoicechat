package com.moro.vkvoicechat.vk;

import com.moro.vkvoicechat.model.pojo.container.*;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 10.01.14
 * Time: 17:48
 */
public interface VkApiMessages {

    @GET("/messages.getDialogs")
    void getDialogs(@Query("offset") long offset, @Query("count") int count,
                             @Query("preview_length") int previewLength, @Query("user_id") long userId,
                             Callback<ResponseEntity<GetDialogsResponseContainer>> callback);

    @GET("/execute.getDialogsAndProfiles")
    void getDialogsAndProfiles(@Query("offset") long offset, @Query("count") int count,
                    @Query("preview_length") int previewLength, @Query("user_id") long userId,
                    Callback<ResponseEntity<GetDialogsAndProfilesResponseContainer>> callback);

    @GET("/messages.getHistory")
    void getHistory(@Query("offset") long offset, @Query("count") int count, @Query("user_id") long userId,
                    @Query("chat_id") long chatId, @Query("start_message_id") long startMessageId, @Query("rev") int rev,
                    Callback<ResponseEntity<GetHistoryResponseContainer>> callback);

    @POST("/messages.send")
    void send(@Query("user_id") long userId, @Query("message") String message, Callback<ResponseEntity<PostMessageResponseContainer>> callback);
}
