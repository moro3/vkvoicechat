package com.moro.vkvoicechat.vk;

import com.moro.vkvoicechat.util.Utils;
import com.moro.vkvoicechat.util.VkApiException;
import com.moro.vkvoicechat.util.logger.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 09.01.14
 * Time: 15:25
 */
public class VkAuth {

    private static final String TAG = VkAuth.class.getSimpleName();

    public static final String REDIRECT_URI = "https://oauth.vk.com/blank.html";

    private static final String AUTH_BASE_URL = "https://oauth.vk.com/authorize?";

    public static String getAuthRequestUrl() {
        return AUTH_BASE_URL
                + "client_id=" + VkApi.getAppId()
                + "&scope=" + VkApi.getPermissions()
                + "&redirect_uri=" + REDIRECT_URI
                + "&display=mobile"
                + "&v=" + VkApiSettings.API_VERSION
                + "&response_type=token";
    }

    public static String[] parseRedirectUri(String redirectUri) throws VkApiException {
        //url is something like http://api.vk.com/blank.html#access_token=66e8f7a266af0dd477fcd3916366b17436e66af77ac352aeb270be99df7deeb&expires_in=0&user_id=7657164
        String accessToken = Utils.extractPattern(redirectUri, "access_token=(.*?)&");
        Logger.i(TAG, "access_token=" + accessToken);
        String userId = Utils.extractPattern(redirectUri, "user_id=(\\d*)");
        Logger.i(TAG, "user_id=" + userId);
        if (userId == null || userId.length() == 0 || accessToken == null || accessToken.length() == 0)
            throw new VkApiException("Failed to parse redirect url " + redirectUri);
        return new String[]{accessToken, userId};
    }

}
