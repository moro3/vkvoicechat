package com.moro.vkvoicechat.vk;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.moro.vkvoicechat.VkApiMessagesManager;
import com.moro.vkvoicechat.util.JacksonConverter;
import com.moro.vkvoicechat.util.MixInModule;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 09.01.14
 * Time: 13:34
 */
public class VkApi {

    private static String accessToken;
    private static String permissions;
    private static String appId;
    public static VkApiMessagesManager messages;

    public static void init(String accessToken, String appId, String permissions) {
        VkApi.accessToken = accessToken;
        VkApi.appId = appId;
        VkApi.permissions = permissions;
        ObjectMapper jacksonMapper = new ObjectMapper();
        jacksonMapper.registerModule(new MixInModule());
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setServer(VkApiSettings.BASE_URL)
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addQueryParam("access_token", getAccessToken());
                        request.addQueryParam("v", VkApiSettings.API_VERSION);
                    }
                }).setConverter(new JacksonConverter(jacksonMapper))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        messages = new VkApiMessagesManager(restAdapter.create(VkApiMessages.class));
    }

    public static boolean isInited() {
        return !accessToken.isEmpty() && !appId.isEmpty();
    }

    public static String getPermissions() {
        return permissions;
    }

    public static String getAppId() {
        return appId;
    }

    public static void setAccessToken(String accessToken) {
        VkApi.accessToken = accessToken;
    }

    public static String getAccessToken() {
        return accessToken;
    }
}
