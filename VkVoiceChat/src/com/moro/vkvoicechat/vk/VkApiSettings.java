package com.moro.vkvoicechat.vk;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 09.01.14
 * Time: 14:50
 */
public interface VkApiSettings {

    public static final String API_VERSION = "5.5";

    public static final String BASE_URL = "https://api.vk.com/method/";

}
