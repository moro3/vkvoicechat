package com.moro.vkvoicechat.net;

import com.moro.vkvoicechat.core.CachingDataReadyCallback;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 24.01.14
 * Time: 15:51
 */
public class FileDownloadRetrofitCallback implements Callback<Response> {

    FileDownloadCallback callback;
    CachingDataReadyCallback<Response> cachingDataReadyCallback;

    public FileDownloadRetrofitCallback(FileDownloadCallback callback, CachingDataReadyCallback<Response> cachingDataReadyCallback) {
        this.cachingDataReadyCallback = cachingDataReadyCallback;
        this.callback = callback;
    }

    @Override
    public void success(Response response, Response response2) {
        cachingDataReadyCallback.cacheData(response);
        callback.onSuccess();
    }

    @Override
    public void failure(RetrofitError error) {
        callback.onFailure(error);
    }
}
