package com.moro.vkvoicechat.net.method;

import com.moro.vkvoicechat.model.pojo.container.ResponseEntity;
import com.moro.vkvoicechat.net.pojo.PostVoiceMessageResponseContainer;
import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.*;
import retrofit.mime.TypedFile;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 16.01.14
 * Time: 18:31
 */
public interface ApiVoiceMessage {

    @Multipart
    @POST("/save")
    public void save(@Part("from") long fromId, @Part("to") long toId, @Part("dur") int duration,
                              @Part("file") TypedFile voiceMessage, @Part("token") String token,
                              Callback<ResponseEntity<PostVoiceMessageResponseContainer>> callback);

    @GET("/download/{user_id}/{token}/{message_id}")
    public void download(@Path("user_id") String userId, @Path("message_id") String messageId, @Path("token") String token, Callback<Response> callback);

}
