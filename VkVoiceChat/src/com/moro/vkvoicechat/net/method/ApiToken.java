package com.moro.vkvoicechat.net.method;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 26.01.14
 * Time: 19:29
 */
public interface ApiToken {

    @Multipart
    @POST("/register")
    public void register(@Part("user_id") String userId, @Part("token") String token, @Part("version") int version, Callback<Response> callback);

}
