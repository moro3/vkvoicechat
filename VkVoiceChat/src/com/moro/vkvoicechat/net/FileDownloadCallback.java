package com.moro.vkvoicechat.net;

import retrofit.RetrofitError;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 24.01.14
 * Time: 16:13
 */
public abstract class FileDownloadCallback {

    public abstract void onSuccess();

    public abstract void onFailure(RetrofitError error);

    public void onCacheReady(String filePath) {}

}
