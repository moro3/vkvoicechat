package com.moro.vkvoicechat.net.pojo;

import com.moro.vkvoicechat.model.pojo.container.ResponseContainer;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 16.01.14
 * Time: 18:37
 */
public class PostVoiceMessageResponseContainer implements ResponseContainer {

    String id;
    String name;
    String url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
