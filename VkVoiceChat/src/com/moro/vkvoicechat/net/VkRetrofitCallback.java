package com.moro.vkvoicechat.net;

import com.moro.vkvoicechat.VkApiCallback;
import com.moro.vkvoicechat.core.CachingDataReadyCallback;
import com.moro.vkvoicechat.model.pojo.container.ResponseContainer;
import com.moro.vkvoicechat.model.pojo.container.ResponseEntity;
import com.moro.vkvoicechat.util.logger.Logger;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 17.01.14
 * Time: 17:51
 */
public class VkRetrofitCallback<T extends ResponseContainer> implements Callback<ResponseEntity<T>> {

    private static final String TAG = VkRetrofitCallback.class.getSimpleName();

    VkApiCallback<T> callback;
    CachingDataReadyCallback<T> cachingDataCallback;

    public VkRetrofitCallback(VkApiCallback<T> callback) {
        this.callback = callback;
    }

    public VkRetrofitCallback(VkApiCallback<T> callback, CachingDataReadyCallback<T> cachingDataCallback) {
        this.callback = callback;
        this.cachingDataCallback = cachingDataCallback;
    }

    @Override
    public void success(ResponseEntity<T> responseEntity, Response httpResponse) {
        Logger.d(TAG, "Got successful retrofit callback");
        T response = responseEntity.getResponse();
        ResponseEntity.Error error = responseEntity.getError();
        if (response != null) {
            Logger.d(TAG, "Got response " + responseEntity.getResponse().getClass().getSimpleName());
            callback.onSuccess(response);
            if (cachingDataCallback != null) {
                cachingDataCallback.cacheData(response);
            }
        } else if (error != null) {
            Logger.w(TAG, "Received error:");
            Logger.w(TAG, "error_code:" + error.getErrorCode());
            Logger.w(TAG, "error_msg:" + error.getErrorMessage());
            callback.onFailure(error);
        }
    }

    @Override
    public void failure(RetrofitError error) {
        Logger.e(TAG, "Request failed, retrofit error: " + error.getMessage());
    }
}
