package com.moro.vkvoicechat.net;

import com.moro.vkvoicechat.VkApiCallback;
import com.moro.vkvoicechat.core.CacheManager;
import com.moro.vkvoicechat.core.CachingDataReadyCallback;
import com.moro.vkvoicechat.net.method.ApiVoiceMessage;
import com.moro.vkvoicechat.net.pojo.PostVoiceMessageResponseContainer;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 23.01.14
 * Time: 17:04
 */
public class ApiVoiceMessageManager {

    ApiVoiceMessage voiceMessage;

    public ApiVoiceMessageManager(ApiVoiceMessage voiceMessage) {
        this.voiceMessage = voiceMessage;
    }

    public void save(long fromId, long toId, int duration,TypedFile voiceMessageFile, String token,
                     VkApiCallback<PostVoiceMessageResponseContainer> callback) {
        voiceMessage.save(fromId, toId, duration, voiceMessageFile, token, new VkRetrofitCallback<PostVoiceMessageResponseContainer>(callback, new Save(voiceMessageFile.fileName())));
    }

    public void download(String userId, String voiceMessageId, String token, FileDownloadCallback callback) {
        String voiceMessageFilePath = CacheManager.getVoiceMessageFileFromCache(voiceMessageId);
        if (voiceMessageFilePath != null) {
            callback.onCacheReady(voiceMessageFilePath);
        } else {
            voiceMessage.download(userId, voiceMessageId, token, new FileDownloadRetrofitCallback(callback, new Download(voiceMessageId)));
        }
    }


    /** ================================= Cache callbacks =============================================== **/

    class Save implements CachingDataReadyCallback<PostVoiceMessageResponseContainer> {

        String tempVoiceMessageFilename;

        Save(String tempVoiceMessageFilename) {
            this.tempVoiceMessageFilename = tempVoiceMessageFilename;
        }

        @Override
        public void cacheData(PostVoiceMessageResponseContainer dataContainer) {
            CacheManager.addVoiceMessageToCache(dataContainer, tempVoiceMessageFilename);

        }
    }

    class Download implements CachingDataReadyCallback<Response> {

        String voiceMessageFileName;

        Download(String voiceMessageFileName) {
            this.voiceMessageFileName = voiceMessageFileName;
        }

        @Override
        public void cacheData(Response dataContainer) {
            CacheManager.addVoiceMessageFileToCache(dataContainer, voiceMessageFileName);
        }
    }

}
