package com.moro.vkvoicechat.net;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.moro.vkvoicechat.net.method.ApiToken;
import com.moro.vkvoicechat.net.method.ApiVoiceMessage;
import com.moro.vkvoicechat.util.JacksonConverter;
import com.moro.vkvoicechat.util.MixInModule;
import retrofit.RestAdapter;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 16.01.14
 * Time: 18:32
 */
public class Api {

    public static ApiVoiceMessageManager voiceMessage;
    public static ApiToken token;

    public static void init() {
        ObjectMapper jacksonMapper = new ObjectMapper();
        jacksonMapper.registerModule(new MixInModule());
        RestAdapter adapter = new RestAdapter.Builder()
                .setServer(ApiSettings.BASE_URL)
                .setConverter(new JacksonConverter(jacksonMapper))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        voiceMessage = new ApiVoiceMessageManager(adapter.create(ApiVoiceMessage.class));
        token = adapter.create(ApiToken.class);
    }

}
