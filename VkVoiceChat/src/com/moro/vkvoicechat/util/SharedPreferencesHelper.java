package com.moro.vkvoicechat.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import static com.moro.vkvoicechat.util.SharedPreferencesHelper.SharedPreferencesKeys.*;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 09.01.14
 * Time: 14:27
 */
public class SharedPreferencesHelper {

    private static String CURRENT_USER = "current_user";

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences currentUserSharedPreferences;

    public static void init(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        currentUserSharedPreferences = context.getSharedPreferences(CURRENT_USER, Context.MODE_PRIVATE);
    }

    public static void saveAccessToken(String accessToken) {
        sharedPreferences.edit().putString(ACCESS_TOKEN, accessToken).commit();
    }

    public static String loadAccessToken() {
        return sharedPreferences.getString(ACCESS_TOKEN, "");
    }

    public static void saveUserId(long userId) {
        currentUserSharedPreferences.edit().putLong(USER_ID, userId).commit();
    }

    public static long loadUserId() {
        return currentUserSharedPreferences.getLong(USER_ID, 0);
    }

    public static void saveUserName(String firstName, String lastName) {
        currentUserSharedPreferences.edit().putString(USER_NAME, firstName + ' ' + lastName).commit();
    }

    public static String loadUserName() {
        return currentUserSharedPreferences.getString(USER_NAME, "");
    }

    public static void saveUserPhoto50(String photo50Url) {
        currentUserSharedPreferences.edit().putString(USER_PHOTO_50, photo50Url).commit();
    }

    public static String loadUserPhoto50() {
        return currentUserSharedPreferences.getString(USER_PHOTO_50, "");
    }

    public static void saveUserPhoto100(String photo100Url) {
        currentUserSharedPreferences.edit().putString(USER_PHOTO_100, photo100Url).commit();
    }

    public static String loadUserPhoto100() {
        return currentUserSharedPreferences.getString(USER_PHOTO_100, "");
    }

    public static void saveUserPhoto200(String photo200Url) {
        currentUserSharedPreferences.edit().putString(USER_PHOTO_200, photo200Url).commit();
    }

    public static String loadUserPhoto200() {
        return currentUserSharedPreferences.getString(USER_PHOTO_200, "");
    }

    interface SharedPreferencesKeys {
        String ACCESS_TOKEN = "access_token";
        String USER_ID = "user_id";
        String USER_NAME = "user_name";
        String USER_PHOTO_50 = "user_photo_50";
        String USER_PHOTO_100 = "user_photo_100";
        String USER_PHOTO_200 = "user_photo_200";
    }

}
