package com.moro.vkvoicechat.util;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.moro.vkvoicechat.model.pojo.Message;
import com.moro.vkvoicechat.model.pojo.container.ResponseEntity;
import com.moro.vkvoicechat.model.pojo.helpers.JSONMap;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 15.01.14
 * Time: 17:24
 */
public class MixInModule extends SimpleModule {

    @Override
    public void setupModule(SetupContext context) {
        context.setMixInAnnotations(Message.class, MessageMixIn.class);
        context.setMixInAnnotations(ResponseEntity.Error.class, ErrorMixIn.class);
    }

    public interface BaseMixIn {

    }

    //TODO Remove this and implement attachments and fwd_messages (and fix chat_active)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public abstract class MessageMixIn extends Message implements BaseMixIn {

        @JsonIgnore
        public abstract Long getId();

        @JsonIgnore
        public abstract void setId(Long id);

        @JsonProperty(value = "id")
        public abstract Long getMessageId();

        @JsonProperty(value = "id")
        public abstract void setMessageId(Long messageId);

        @JsonProperty(value = "user_id")
        public abstract Long getUserId();

        @JsonProperty(value = "user_id")
        public abstract void setUserId(Long userId);

        @JsonProperty(value = "from_id")
        public abstract Long getFromId();

        @JsonProperty(value = "from_id")
        public abstract void setFromId(Long fromId);

        @JsonProperty(value = "read_state")
        public abstract Integer getReadState();

        @JsonProperty(value = "read_state")
        public abstract void setReadState(Integer readState);

        @JsonProperty(value = "chat_id")
        public abstract Long getChatId();

        @JsonProperty(value = "chat_id")
        public abstract void setChatId(Long chatId);

        @JsonProperty(value = "chat_active")
        public abstract long[] getChatActive();

        @JsonProperty(value = "chat_active")
        public abstract void setChatActive(long[] chatActive);

        @Override
        @JsonIgnore
        public abstract byte[] getAttachments();

        @Override
        @JsonIgnore
        public abstract void setAttachments(byte[] attachments);

        @Override
        @JsonIgnore
        public abstract byte[] getFwdMessages();

        @Override
        @JsonIgnore
        public abstract void setFwdMessages(byte[] fwdMessages);

        @JsonProperty(value = "users_count")
        public abstract Long getUsersCount();

        @JsonProperty(value = "users_count")
        public abstract void setUsersCount(Long usersCount);

        @JsonProperty(value = "admin_id")
        public abstract Long getAdminId();

        @JsonProperty(value = "admin_id")
        public abstract void setAdminId(Long adminId);

        @JsonProperty(value = "photo_50")
        public abstract String getPhoto50();

        @JsonProperty(value = "photo_50")
        public abstract void setPhoto50(String photo50);

        @JsonProperty(value = "photo_100")
        public abstract String getPhoto100();

        @JsonProperty(value = "photo_100")
        public abstract void setPhoto100(String photo100);

        @JsonProperty(value = "photo_200")
        public abstract String getPhoto200();

        @JsonProperty(value = "photo_200")
        public abstract void setPhoto200(String photo200);

    }

    public abstract class ErrorMixIn extends ResponseEntity.Error implements BaseMixIn {

        @Override
        @JsonProperty(value = "error_code")
        public abstract int getErrorCode();

        @Override
        @JsonProperty(value = "error_code")
        public abstract void setErrorCode(int errorCode);

        @Override
        @JsonProperty(value = "error_msg")
        public abstract String getErrorMessage();

        @Override
        @JsonProperty(value = "error_msg")
        public abstract void setErrorMessage(String errorMessage);

        @Override
        @JsonProperty(value = "request_params")
        public abstract List<JSONMap> getRequestParams();

        @Override
        @JsonProperty(value = "request_params")
        public abstract void setRequestParams(List<JSONMap> requestParams);
    }

}
