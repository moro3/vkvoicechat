package com.moro.vkvoicechat.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 09.01.14
 * Time: 15:21
 */
public class Utils {

    public static String extractPattern(String string, String patternString){
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(string);
        if (!matcher.find())
            return null;
        return matcher.toMatchResult().group(1);
    }

}
