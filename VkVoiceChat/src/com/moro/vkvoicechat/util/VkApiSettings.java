package com.moro.vkvoicechat.util;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 09.01.14
 * Time: 14:50
 */
public interface VkApiSettings {

    public static final String APP_ID = "4102718";
    public static final String PERMISSIONS = "friends,messages,offline";
    public static final String VK_APP_URL = "http://vk.com/app4120087";
    public static final String VK_APP_URL_ESCAPED = "http:\\/\\/vk\\.com\\/app4120087";

}
