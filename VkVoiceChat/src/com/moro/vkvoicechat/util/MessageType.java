package com.moro.vkvoicechat.util;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 16.01.14
 * Time: 18:04
 */
public enum MessageType {

    MESSAGE_TYPE_TEXT_TO_ME, MESSAGE_TYPE_TEXT_FROM_ME, MESSAGE_TYPE_VOICE
}
