package com.moro.vkvoicechat.util.logger;

import android.util.Log;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 08.01.14
 * Time: 14:41
 */
public class Logger {

    private static LoggingLevel loggingLevel = LoggingLevel.VERBOSE;

    public static int v(String tag, String msg) {
        if (loggingLevel == LoggingLevel.VERBOSE) {
            return Log.v(tag, msg);
        } else {
            return 0;
        }
    }

    public static int v(String tag, String msg, Throwable tr) {
        if (loggingLevel == LoggingLevel.VERBOSE) {
            return Log.v(tag, msg, tr);
        } else {
            return 0;
        }
    }

    public static int d(String tag, String msg) {
        if (loggingLevel.ordinal() <= LoggingLevel.DEBUG.ordinal()) {
            return Log.d(tag, msg);
        } else {
            return 0;
        }
    }

    public static int d(String tag, String msg, Throwable tr) {
        if (loggingLevel.ordinal() <= LoggingLevel.DEBUG.ordinal()) {
            return Log.d(tag, msg, tr);
        } else {
            return 0;
        }
    }

    public static int i(String tag, String msg) {
        if (loggingLevel.ordinal() <= LoggingLevel.INFO.ordinal()) {
            return Log.i(tag, msg);
        } else {
            return 0;
        }
    }

    public static int i(String tag, String msg, Throwable tr) {
        if (loggingLevel.ordinal() <= LoggingLevel.INFO.ordinal()) {
            return Log.i(tag, msg, tr);
        } else {
            return 0;
        }
    }

    public static int w(String tag, String msg) {
        if (loggingLevel.ordinal() <= LoggingLevel.WARNING.ordinal()) {
            return Log.w(tag, msg);
        } else {
            return 0;
        }
    }

    public static int w(String tag, String msg, Throwable tr) {
        if (loggingLevel.ordinal() <= LoggingLevel.WARNING.ordinal()) {
            return Log.w(tag, msg, tr);
        } else {
            return 0;
        }
    }

    public static int e(String tag, String msg) {
        if (loggingLevel.ordinal() <= LoggingLevel.ERROR.ordinal()) {
            return Log.e(tag, msg);
        } else {
            return 0;
        }
    }

    public static int e(String tag, String msg, Throwable tr) {
        if (loggingLevel.ordinal() <= LoggingLevel.ERROR.ordinal()) {
            return Log.e(tag, msg, tr);
        } else {
            return 0;
        }
    }

    public static void setLogLevel(LoggingLevel loggingLevel) {
        Logger.loggingLevel = loggingLevel;
    }

}
