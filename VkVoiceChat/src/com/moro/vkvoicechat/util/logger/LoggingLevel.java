package com.moro.vkvoicechat.util.logger;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 08.01.14
 * Time: 14:45
 */
public enum LoggingLevel {
    VERBOSE, DEBUG, INFO, WARNING, ERROR, QUIET
}
