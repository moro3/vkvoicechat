package com.moro.vkvoicechat.util;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 09.01.14
 * Time: 16:42
 */
public class VkApiException extends Exception {

    public VkApiException() {
        super();
    }

    public VkApiException(String detailMessage) {
        super(detailMessage);
    }

    public VkApiException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public VkApiException(Throwable throwable) {
        super(throwable);
    }
}
