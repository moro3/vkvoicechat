package com.moro.vkvoicechat;

import com.moro.vkvoicechat.model.pojo.container.ResponseContainer;
import com.moro.vkvoicechat.model.pojo.container.ResponseEntity;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 17.01.14
 * Time: 17:07
 */
public abstract class VkApiCallback<T extends ResponseContainer> {

    public abstract void onSuccess(T response);

    public abstract void onFailure(ResponseEntity.Error error);

    public void onCacheReady(T cacheResponse) {}

}
