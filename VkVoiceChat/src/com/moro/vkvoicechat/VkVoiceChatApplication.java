package com.moro.vkvoicechat;

import android.app.Application;
import android.content.Context;
import com.moro.vkvoicechat.core.CacheManager;
import com.moro.vkvoicechat.net.Api;
import com.moro.vkvoicechat.util.SharedPreferencesHelper;
import com.moro.vkvoicechat.util.VkApiSettings;
import com.moro.vkvoicechat.vk.VkApi;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 06.01.14
 * Time: 16:31
 */
public class VkVoiceChatApplication extends Application {

    static Application instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(options)
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .discCacheSize(50 * 1024 * 1024)
                .discCacheFileCount(100)
                .writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(config);
        CacheManager.init();
        SharedPreferencesHelper.init(this);
        VkApi.init(SharedPreferencesHelper.loadAccessToken(), VkApiSettings.APP_ID, VkApiSettings.PERMISSIONS);
        Api.init();
    }

    public static Context getAppContext() {
        return instance;
    }
}
