package com.moro.vkvoicechat;

import com.moro.vkvoicechat.core.CacheManager;
import com.moro.vkvoicechat.core.CachingDataReadyCallback;
import com.moro.vkvoicechat.model.pojo.container.GetDialogsAndProfilesResponseContainer;
import com.moro.vkvoicechat.model.pojo.container.GetDialogsResponseContainer;
import com.moro.vkvoicechat.model.pojo.container.GetHistoryResponseContainer;
import com.moro.vkvoicechat.model.pojo.container.PostMessageResponseContainer;
import com.moro.vkvoicechat.net.VkRetrofitCallback;
import com.moro.vkvoicechat.vk.VkApiMessages;

/**
 * Created with IntelliJ IDEA.
 * User: mc975
 * Date: 17.01.14
 * Time: 15:59
 */
public class VkApiMessagesManager {

    VkApiMessages messages;

    public VkApiMessagesManager(VkApiMessages messages) {
        this.messages = messages;
    }

    public void getDialogs(long offset, int count, int previewLength, long userId, VkApiCallback<GetDialogsResponseContainer> callback) {
        messages.getDialogs(offset, count, previewLength,userId, new VkRetrofitCallback<GetDialogsResponseContainer>(callback,
                new GetDialogsCachingCallback(offset, count, previewLength, userId)));
        GetDialogsResponseContainer cacheResponse = CacheManager.loadGetDialogsFromDb(offset, count, previewLength, userId);
        if (cacheResponse != null) {
            callback.onCacheReady(cacheResponse);
        }
    }

    public void getDialogs(int count, VkApiCallback<GetDialogsResponseContainer> callback) {
        getDialogs(0, count, 0, 0, callback);
    }

    public void getDialogsAndProfiles(long offset, int count, int previewLength, long userId, VkApiCallback<GetDialogsAndProfilesResponseContainer> callback) {
        messages.getDialogsAndProfiles(offset, count, previewLength, userId, new VkRetrofitCallback<GetDialogsAndProfilesResponseContainer>(callback,
                new GetDialogsAndProfilesCachingCallback(offset, count, previewLength, userId)));
        GetDialogsAndProfilesResponseContainer cacheResponse = CacheManager.loadGetDialogsAndProfilesFromDb(offset, count, previewLength, userId);
        if (cacheResponse != null) {
            callback.onCacheReady(cacheResponse);
        }
    }

    public void getDialogsAndProfiles(int count, VkApiCallback<GetDialogsAndProfilesResponseContainer> callback) {
        getDialogsAndProfiles(0, count, 0, 0, callback);
    }


    public void getHistory(long offset, int count, long userId, long chatId, long startMessageId, int rev, VkApiCallback<GetHistoryResponseContainer> callback) {
        messages.getHistory(offset, count, userId, chatId, startMessageId, rev, new VkRetrofitCallback<GetHistoryResponseContainer>(callback,
                new GetHistoryCachingCallback(offset, count, userId, startMessageId, rev)));
        GetHistoryResponseContainer cacheResponse = CacheManager.loadGetHistoryFromDb(offset, count, userId, startMessageId, rev);
        if (cacheResponse != null) {
            callback.onCacheReady(cacheResponse);
        }
    }

    public void getHistory(int count, long userId, VkApiCallback<GetHistoryResponseContainer> callback) {
        getHistory(0, count, userId, 0, 0, 0, callback);
    }

    public void getChatHistory(int count, long chatId, VkApiCallback<GetHistoryResponseContainer> callback) {
        getHistory(0, count, 0, chatId, 0, 0, callback);
    }

    public void send(long userId, String message, VkApiCallback<PostMessageResponseContainer> callback) {
        messages.send(userId, message, new VkRetrofitCallback<PostMessageResponseContainer>(callback));
    }



    /** ================================= Cache callbacks =============================================== **/

    class GetDialogsCachingCallback implements CachingDataReadyCallback<GetDialogsResponseContainer> {

        long offset;
        int count;
        int previewLength;
        long userId;

        GetDialogsCachingCallback(long offset, int count, int previewLength, long userId) {
            this.offset = offset;
            this.count = count;
            this.previewLength = previewLength;
            this.userId = userId;
        }

        @Override
        public void cacheData(GetDialogsResponseContainer dataContainer) {
            CacheManager.saveGetDialogsToDb(dataContainer, offset, count, previewLength, userId);
        }
    }

    class GetDialogsAndProfilesCachingCallback implements CachingDataReadyCallback<GetDialogsAndProfilesResponseContainer> {

        long offset;
        int count;
        int previewLength;
        long userId;

        GetDialogsAndProfilesCachingCallback(long offset, int count, int previewLength, long userId) {
            this.offset = offset;
            this.count = count;
            this.previewLength = previewLength;
            this.userId = userId;
        }

        @Override
        public void cacheData(GetDialogsAndProfilesResponseContainer dataContainer) {
            CacheManager.saveGetDialogsAndProfilesToDb(dataContainer, offset, count, previewLength, userId);
        }
    }

    class GetHistoryCachingCallback implements CachingDataReadyCallback<GetHistoryResponseContainer> {

        long offset;
        int count;
        long userId;
        long startMessageId;
        int rev;

        GetHistoryCachingCallback(long offset, int count, long userId, long startMessageId, int rev) {
            this.offset = offset;
            this.count = count;
            this.userId = userId;
            this.startMessageId = startMessageId;
            this.rev = rev;
        }

        @Override
        public void cacheData(GetHistoryResponseContainer dataContainer) {
            CacheManager.saveGetHistoryToDb(dataContainer, offset, count, userId, startMessageId, rev);
        }
    }

}
